import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import Images from '../Images';
import styles from './styles';

interface Props {
  image: string;
  name: string;
  age: string;
  onClickEdit: () => void;
  onClcikDelete: () => void;
}

const CardProfile = ({
  image = '',
  name = '',
  age = '',
  onClcikDelete = () => {},
  onClickEdit = () => {},
}: Props) => {
  return (
    <View style={styles.container}>
      <Images url={image} />
      <Text style={styles.title}>{name}</Text>
      <Text style={styles.subTitle}>{age} Tahun</Text>
      <TouchableOpacity
        onPress={onClcikDelete}
        style={styles.containerImageDelete}>
        <Image
          style={styles.imageAction}
          source={require('../../assets/image/deleteIcon.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={onClickEdit} style={styles.containerImageEdit}>
        <Image
          style={styles.imageAction}
          source={require('../../assets/image/penIcon.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

export default CardProfile;
