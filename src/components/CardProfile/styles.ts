import {Dimensions, StyleSheet} from 'react-native';
const SCREEN_WIDTH = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  container: {
    width: (SCREEN_WIDTH - 32) / 2,
    height: 270,
    margin: 8,
    paddingBottom: 8,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#808080',
    borderRadius: 12,
  },
  imageCard: {
    width: '100%',
    height: 200,
    borderRadius: 12,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 24,
    marginTop: 8,
    marginLeft: 8,
    color: '#000',
  },
  subTitle: {
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 20,
    marginTop: 4,
    marginLeft: 8,
    color: '#000',
  },
  containerImageDelete: {
    width: 24,
    position: 'absolute',
    top: 0,
    right: 0,
    margin: 16,
  },
  containerImageEdit: {
    width: 24,
    position: 'absolute',
    top: 0,
    right: 0,
    marginRight: 48,
    marginTop: 16,
  },
  imageAction: {
    width: 20,
    height: 20,
  },
});

export default styles;
