import {Dimensions, StyleSheet} from 'react-native';
const SCREEN_WIDTH = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  rowList: {
    flex: 1,
    justifyContent: 'space-between',
  },
  containerEmpty: {
    flex: 1,
    marginTop: '60%',
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  contentModal: {
    backgroundColor: '#fff',
    width: SCREEN_WIDTH - 38,
    padding: 16,
    paddingBottom: 32,
    borderRadius: 12,
  },
  containerHeaderModal: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  titleModal: {
    fontSize: 18,
    fontWeight: '600',
    color: '#000',
  },
  imageClose: {
    width: 18,
    height: 18,
    marginTop: 4,
  },
  input: {
    height: 44,
    marginTop: 16,
    borderWidth: 1,
    padding: 10,
    borderRadius: 12,
  },
  buttonModal: {
    borderRadius: 12,
    marginTop: 24,
  },
});

export default styles;
